<?php include ('server.php');?>
<!DOCTYPE html>
<html>
<head>
  <title>Yang MEH MEH</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
 <link rel="stylesheet" href="css2.css">
</head>
<body>
  
    <div class ="container">
        <div class="row">
            <div class="col-md-4 offset-md-4 form-div">
                <form method="post" action="register.php">
                    <h3 class="text-center">User Sign Up</h3>
                    
                    <?php if(count($errors)>0):?>
                    <div class="alert alert-danger">
                        <?php foreach($errors as $error):?>
                        <li><?php echo $error;?></li>
                        <?php endforeach;?>
                    </div>
                    <?php endif;?>
                    
                    <div class="form-group">
                        <label>Username </label>
                        <input placeholder="eg:zc123" type="text" name="username" class="form-control form-control-lg" value="<?php echo $username; ?>">
                    </div>
        
                    <div class="form-group">
                      <label>Full Name</label>
                      <input type="text" name="name" class="form-control form-control-lg" value="<?php echo $name; ?>">
                    </div>
                    
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control form-control-lg" value="<?php echo $email; ?>">
                    </div>
                    
                    <div class="form-group">
                        <label>Address </label>
                        <input placeholder="eg:zc123" type="text" name="address" class="form-control form-control-lg" value="<?php echo $username; ?>">
                    </div>
                    
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" name="password1"class="form-control form-control-lg">
                    </div>
                    
                    <div class="form-group">
                      <label>Confirm password</label>
                      <input type="password" name="password2" class="form-control form-control-lg">
                    </div>
                    
                    <div class="form-group">
                        <button type="submit"  name="reg_user" class="btn btn-primary btn-block btn-lg">Register</button>
                    </div>
                    
                    <p>
                            Already a member? <a href="login.php">Sign in</a>
                    </p>
        
                </form>
            </div>
        </div>
    </div>
  
</body>
</html>