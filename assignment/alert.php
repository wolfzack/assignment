
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css2.css">
        <title>Homepage</title>
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 form-div login">
                    <div class="alert alert-success">
                        You are logged in!
                    </div>
                    <h3>Welcome!</h3>
                    <a href="#" class="logout">Logout</a>
                    
                    <div class="alert alert-success">
                        Thanks for registering as an user.
                        
                    </div>
                    
                    <button class="btn btn-block btn-lg btn-primary" onclick="location.href='index.php'">Go to Home page</button>
                </div>
            </div>
        </div>
        
    </body>
</html>
