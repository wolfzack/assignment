

<?php
session_start();

require 'database.php';

$id ="";
$name ="";
$email ="";
$errors = array();

$con = mysqli_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB) or
        die ('Unable to connect Database');

//register user
if(isset($_POST['reg_user'])){
    $id =$_POST['username'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $address=$_POST['address'];
    $password1 =$_POST['password1'];
    $password2 =$_POST['password2'];
    
    
    if(empty($username)){
        $errors['username']='Username is required.';
    }
    
    $idquery = "SELECT * FROM user WHERE username = ? LIMIT 1";
    $stmt = $con ->prepare($idquery);
    $stmt -> bind_param('s',$id);
    $stmt -> execute();
    $result = $stmt ->get_result();
    $usercount = $result ->num_rows;
    $stmt->close();
    
  
    
    if($usercount>0){
        $errors['id'] = "Student ID already exist!";
    }
    
    if(empty($name)){
       $errors['name']= 'Name is required.';
    }
    
    if(empty($email)){
        $errors['email']= 'Email address required.';
    }
    
    $emailquery = "SELECT * FROM user WHERE email =? LIMIT 1";
    $stmt = $con ->prepare($emailquery);
    $stmt -> bind_param('s',$email);
    $stmt -> execute();
    $result = $stmt ->get_result();
    $usercount = $result ->num_rows;
    $stmt->close();
    
    if($usercount >0){
        $errors['email'] ="Email already exists!";
    }
    
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
      $errors['email']= 'Email address invalid.';
    }
    
    if(empty($address)){
       $errors['address']= 'Address is required.';
    }
    
    if(empty($password1)){
       $errors['password1']= 'Password is required.';
    }
    
    if(empty($password2)){
       $errors['password2']= 'Password confirmation is required.';
    }
    
    if ($password2 !== $password1){
        $errors['password2']="Passwotd not matched.";
    }
    
    
    
    
    
    if(count($errors)===0){
        $password = password_hash($password1, PASSWORD_DEFAULT);
        $token = bin2hex(random_bytes(50));
        $verified = false;
        $sql = "INSERT INTO user (username,email,name,address,password) 
                VALUES (?,?,?,?,?)";
        
        $stmt = $con ->prepare($sql);
        $stmt -> bind_param('sssss',$id,$name,$email,$password);
        
       if($stmt ->execute()){
           
            $user_id = $con -> insert_id;
            $_SESSION['username']=$user['username'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['address'] = $user['address'];
            $_SESSION['name']=$user['name'];
            $_SESSION['password'] = $user['password'];
           
           
            $_SESSION['message'] = "You are logged in!";
            $_SESSION['alert-class'] = "alert-success";          
            header('location: index.php');
            exit();
        }
       else{
           $errors ['db_error'] = "Database error : failed to register";
       }
    }
}

if(isset($_POST['login-btn'])){
    $eid = $_POST['eid'];
    $password =$_POST['password'];
    
    if(empty($email)){
        $errors['eid']= 'Email address or Username is required.';
    }
    
    if(empty($password)){
        $errors['password']= 'Password is required.';
    }
    
    $sql = "SELECT * FROM user WHERE email = ? or username = ? LIMIT 1";
    $stmt = $con ->prepare($sql);
    $stmt -> bind_param('ss',$eid,$eid);
    $stmt -> execute();
    
    $result = $stmt -> get_result();
    $user = $result -> fetch_assoc();
    
    if (password_verify($password, $user['password'])){
        $_SESSION['username']=$user['username'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['address'] = $user['address'];
        $_SESSION['name']=$user['name'];
        $_SESSION['password'] = $user['password'];
        
        $_SESSION['message'] = "You are now logged in.";
        $_SESSION['alert-class'] = "alert-success";
        header('location : index.php');
        exit();
    }
    else{
        $errors ['login-failed'] = "Wrong credentials!";
    }
    
}

?>
