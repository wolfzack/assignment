<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
  <link rel="stylesheet" type="text/css" href="css2.css">
</head>
<body>
 <div class ="container">
        <div class="row">
            <div class="col-md-4 offset-md-4 form-div">
                <form method="post" action="login.php">
                    <h3 class="text-center">Log IN</h3>
                  
                    
                   <?php if(count($errors)>0):?>
                    <div class="alert alert-danger">
                        <?php foreach($errors as $error):?>
                        <li><?php echo $error;?></li>
                        <?php endforeach;?>
                    </div>
                    <?php endif;?>
                    
                    <div class="form-group">
                      <label>Email or Username</label>
                      <input type="text" name="eid" class="form-control form-control-lg" >
                    </div>
                    
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" name="password"class="form-control form-control-lg">
                    </div>
                    
                   
                    
                    <div class="form-group">
                        <button type="submit"  name="login-btn" class="btn btn-primary btn-block btn-lg">Log IN</button>
                    </div>
                    
                    <p>
                            Not a member yet? <a href="register.php">Sign up</a>
                    </p>
        
                </form>
            </div>
        </div>
    </div>
</body>
</html>